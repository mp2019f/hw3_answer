package kr.ac.mju.mp2019f.smarttipper;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static android.widget.CompoundButton.*;


public class MainActivity extends AppCompatActivity {

    private View BackGround;
    private RadioGroup radioGroup;
    private RadioButton BtNotip, BtRantip, BtBy, BtMax;
    private CheckBox checkBox;
    private SeekBar seekBar;
    private EditText SaleEdit, TipEdit, TotalEdit;
    private TextView[] text = new TextView[9];

    private int tip,sales;

    public boolean isNumeric(String input) {
        try {
            Double.parseDouble(input);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        text[0] = findViewById(R.id.TipText);
        text[1] = findViewById(R.id.S1);
        text[2] = findViewById(R.id.S3);
        text[3] = findViewById(R.id.TotalText);
        text[4] = findViewById(R.id.SaleText);
        text[5] = findViewById(R.id.S2);
        text[6] = findViewById(R.id.RateText);
        text[7] = findViewById(R.id.MinText);
        text[8] = findViewById(R.id.MaxText);


        BackGround = findViewById((R.id.BackGround));
        radioGroup = findViewById(R.id.radioGroup);
        BtNotip = findViewById(R.id.BtNotip);
        BtRantip = findViewById(R.id.BtRantip);
        BtBy = findViewById(R.id.BtBy);
        BtMax = findViewById(R.id.BtMax);
        checkBox = findViewById(R.id.checkBox);
        seekBar = findViewById(R.id.seekBar);
        TipEdit = findViewById(R.id.TipEdit);
        SaleEdit = findViewById(R.id.SaleEdit);
        TotalEdit = findViewById(R.id.TotalEdit);

        text[6].setText("Rate :  0 %");


        checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true){
                    BackGround.setBackgroundColor(Color.BLACK);
                    for(int i = 0; i< 9 ; i++)
                        text[i].setTextColor(Color.WHITE);
                    BtBy.setBackgroundColor(Color.DKGRAY);
                    BtMax.setBackgroundColor(Color.DKGRAY);
                    BtNotip.setBackgroundColor(Color.DKGRAY);
                    BtRantip.setBackgroundColor(Color.DKGRAY);
                    checkBox.setBackgroundColor(Color.DKGRAY);
                    BtBy.setTextColor(Color.WHITE);
                    BtMax.setTextColor(Color.WHITE);
                    BtNotip.setTextColor(Color.WHITE);
                    BtRantip.setTextColor(Color.WHITE);
                    checkBox.setTextColor(Color.WHITE);

                    TipEdit.setTextColor(Color.WHITE);
                    SaleEdit.setTextColor(Color.WHITE);
                    TotalEdit.setTextColor(Color.WHITE);
                    TipEdit.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                    SaleEdit.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                    TotalEdit.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                    seekBar.setBackgroundColor(Color.DKGRAY);
                }
                else{
                    BackGround.setBackgroundColor(Color.WHITE);
                    for(int i = 0; i < 9; i++)
                        text[i].setTextColor(Color.GRAY);
                    BtBy.setBackgroundColor(Color.WHITE);
                    BtMax.setBackgroundColor(Color.WHITE);
                    BtNotip.setBackgroundColor(Color.WHITE);
                    BtRantip.setBackgroundColor(Color.WHITE);
                    checkBox.setBackgroundColor(Color.WHITE);
                    BtBy.setTextColor(Color.GRAY);
                    BtMax.setTextColor(Color.GRAY);
                    BtNotip.setTextColor(Color.GRAY);
                    BtRantip.setTextColor(Color.GRAY);
                    checkBox.setTextColor(Color.GRAY);
                    TipEdit.setTextColor(Color.GRAY);
                    SaleEdit.setTextColor(Color.GRAY);
                    TotalEdit.setTextColor(Color.GRAY);
                    TipEdit.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    SaleEdit.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    TotalEdit.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    seekBar.setBackgroundColor(Color.WHITE);
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(isNumeric(SaleEdit.getText().toString()) == false || Integer.parseInt(SaleEdit.getText().toString()) < 0){
                    Toast.makeText(MainActivity.this,"알맞은 숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                    TipEdit.setText("");
                    TotalEdit.setText("");
                }
                else if(SaleEdit.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                    TipEdit.setText("");
                    TotalEdit.setText("");
                }
                else{
                    text[6].setText("Rate:  "+ String.valueOf(progress) + "%");
                    sales = Integer.parseInt(SaleEdit.getText().toString());
                    tip = (sales * progress) / 100;
                    TipEdit.setText(String.valueOf(tip));
                    TotalEdit.setText(String.valueOf(sales+tip));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        SaleEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(isNumeric(SaleEdit.getText().toString()) == false || Integer.parseInt(SaleEdit.getText().toString()) < 0){
                    Toast.makeText(MainActivity.this,"알맞은 숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                    TipEdit.setText("");
                    TotalEdit.setText("");
                }
                else if(SaleEdit.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                    TipEdit.setText("");
                    TotalEdit.setText("");
                }
                else{
                    text[6].setText("Rate:  "+ String.valueOf(seekBar.getProgress()) + "%");
                    sales = Integer.parseInt(SaleEdit.getText().toString());
                    tip = (sales * seekBar.getProgress()) / 100;
                    TipEdit.setText(String.valueOf(tip));
                    TotalEdit.setText(String.valueOf(sales+tip));
                }

            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.BtNotip:
                        if(isNumeric(SaleEdit.getText().toString()) == false || Integer.parseInt(SaleEdit.getText().toString()) < 0){
                            Toast.makeText(MainActivity.this,"알맞은 숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else if(SaleEdit.getText().toString().equals("")){
                            Toast.makeText(MainActivity.this,"숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else{
                            BtNotip.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sales = Integer.parseInt(SaleEdit.getText().toString());
                                    if(sales < 0 ){
                                        return;
                                    }
                                    text[6].setText("Rate :  0 %");

                                    TipEdit.setText("No Tip");
                                    TotalEdit.setText(String.valueOf(sales));
                                    seekBar.setEnabled(false);
                                }
                            });
                            break;
                        }
                    case R.id.BtRantip:
                        if(isNumeric(SaleEdit.getText().toString()) == false || Integer.parseInt(SaleEdit.getText().toString()) < 0){
                            Toast.makeText(MainActivity.this,"알맞은 숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else if(SaleEdit.getText().toString().equals("")){
                            Toast.makeText(MainActivity.this,"숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else{
                            BtRantip.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sales = Integer.parseInt(SaleEdit.getText().toString());
                                    if(sales < 0){
                                        return;
                                    }
                                    else {
                                        Random Rand = new Random();
                                        int rate = Rand.nextInt(31);
                                        text[6].setText("Rate:  " + String.valueOf(seekBar.getProgress()) + "%");
                                        seekBar.setEnabled(false);
                                        seekBar.setProgress(rate);

                                        tip = (sales * seekBar.getProgress()) / 100;
                                        TipEdit.setText(String.valueOf(tip));
                                        TotalEdit.setText(String.valueOf(sales + tip));
                                    }
                                }
                            });
                            break;
                        }
                    case R.id.BtBy:
                        if(isNumeric(SaleEdit.getText().toString()) == false || Integer.parseInt(SaleEdit.getText().toString()) < 0){
                            Toast.makeText(MainActivity.this,"알맞은 숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else if(SaleEdit.getText().toString().equals("")){
                            Toast.makeText(MainActivity.this,"숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else{
                            BtBy.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sales = Integer.parseInt(SaleEdit.getText().toString());
                                    if(sales < 0){
                                        return;
                                    }
                                    else {
                                        text[6].setText("Rate:  " + String.valueOf(seekBar.getProgress()) + "%");
                                        seekBar.setEnabled(true);
                                        seekBar.setProgress(0);
                                        tip = (sales * seekBar.getProgress()) / 100;
                                        TipEdit.setText(String.valueOf(tip));
                                        TotalEdit.setText(String.valueOf(sales + tip));
                                    }
                                }
                            });
                            break;
                        }

                    case R.id.BtMax:
                        if(isNumeric(SaleEdit.getText().toString()) == false || Integer.parseInt(SaleEdit.getText().toString()) < 0){
                            Toast.makeText(MainActivity.this,"알맞은 숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else if(SaleEdit.getText().toString().equals("")){
                            Toast.makeText(MainActivity.this,"숫자를 입력해주세요",Toast.LENGTH_SHORT).show();
                            TipEdit.setText("");
                            TotalEdit.setText("");
                            break;
                        }
                        else{
                            BtMax.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sales = Integer.parseInt(SaleEdit.getText().toString());
                                    if(sales < 0){
                                        return;
                                    }
                                    else {
                                        text[6].setText("Rate:  " + String.valueOf(seekBar.getProgress()) + "%");
                                        seekBar.setEnabled(false);
                                        seekBar.setProgress(30);
                                        tip = (sales * seekBar.getProgress()) / 100;
                                        TipEdit.setText(String.valueOf(tip));
                                        TotalEdit.setText(String.valueOf(sales + tip));
                                    }
                                }
                            });
                            break;
                        }




                }

            }
        });
    }
}
